$(document).ready(function () {

    $('#table_id').DataTable({
        ajax: {
            url: 'http://localhost:3000/anagrafica',
            dataSrc: '',
        },
        columns: [
            { data: 'id', className: 'id' },
            { data: 'RagioneSociale', className: 'ragione-sociale' },
            { data: 'Indirizzo', className: 'indirizzo' },
            { data: 'Citta', className: 'citta' },
            { data: 'Prov', className: 'provincia' },
            { data: 'CAP', className: 'cap' },
            { data: 'PIVA', className: 'piva' },
            { data: 'CF', className: 'cf' },
            { data: 'Telefono', className: 'telefono' },
            { data: 'Fax', className: 'fax' },
            { data: 'Email', className: 'email' }
        ],
        columnDefs: [
            {
                render:
                    function () {
                        return '<button class="delete-btn btn btn-outline-dark">Delete</button>';
                    },
                targets: 11
            },
            {
                render:
                    function () {
                        return '<button class="edit-btn btn btn-outline-dark">Edit</button>';
                    },
                targets: 12
            }

        ]
    });

    $('#table_id_filter').before('<button class="add-btn btn btn-outline-dark">New</button>')


    var table = $('#table_id');

    //delete button
    table.on('click', '.delete-btn', function () {
        $(this).closest('tr').next('.update-form').remove();
        $(this).closest('tr').remove();
        $('.add-btn').show();
    });

    //edit button, crea un form che mi consente di applicare modifiche alla riga selezionata
    table.on('click', '.edit-btn', function () {

        $('.selected').removeClass('selected');
        $('.edit-btn').show();
        $('.add-btn').hide();
        $('.update-form').remove();

        var tab = $(this);

        let edit = function (position) {
            return tab.closest('tr').find(position).html();
        }

        $(this).closest('tr').after(`<tr class="update-form">
            <form>
            <td></td>
            <td><input type="text" class="ragione-sociale-f" placeholder="Ragione Sociale" value="${edit('td:eq(1)')}"></td>
            <td><input type="text" class="indirizzo-f" placeholder="Indirizzo" value="${edit('td:eq(2)')}"></td>
            <td><input type="text" class="citta-f" placeholder="Città" value="${edit('td:eq(3)')}"></td>
            <td><input type="text" class="provincia-f" placeholder="Provincia" value="${edit('td:eq(4)')}"></td>
            <td><input type="text" class="cap-f" placeholder="CAP" value="${edit('td:eq(5)')}"></td>
            <td><input type="text" class="piva-f" placeholder="PIVA" value="${edit('td:eq(6)')}"></td>
            <td><input type="text" class="cf-f" placeholder="CF" value="${edit('td:eq(7)')}"></td>
            <td><input type="text" class="telefono-f" placeholder="Telefono" value="${edit('td:eq(8)')}"></td>
            <td><input type="text" class="fax-f" placeholder="Fax" value="${edit('td:eq(9)')}"></td>
            <td><input type="text" class="email-f"placeholder="E-mail" value="${edit('td:eq(10)')}"></td>
            <td><input class="form-sub btn btn-outline-dark" type="submit" value="Update"></td>
            <td><input class="cancel-btn btn btn-outline-dark" type="submit" value="Canc"></td>
            <td></td>
            </form>
            </tr>`
        ).addClass('selected');

        $(this).hide();
    });

    //cancel button
    table.on('click', '.cancel-btn', function () {

        $(this).closest('tr').remove();
        $('.delete-btn').removeClass('disabled');
        $('.edit-btn').removeClass('disabled');
        $('.selected').removeClass('selected');
        $('.edit-btn').show();
        $('.add-btn').show();

    })

    //funzione che mi consente di sostituire i valori del tr, con quelli del form 
    //sia quello che aggiorna, che quello nuovo 
    var submitVal = function (referClass) {

        var sel = $(referClass);

        sel.find('.ragione-sociale').text($('.ragione-sociale-f').val());
        sel.find('.indirizzo').text($('.indirizzo-f').val());
        sel.find('.citta').text($('.citta-f').val());
        sel.find('.provincia').text($('.provincia-f').val());
        sel.find('.cap').text($('.cap-f').val());
        sel.find('.piva').text($('.piva-f').val());
        sel.find('.cf').text($('.cf-f').val());
        sel.find('.telefono').text($('.telefono-f').val());
        sel.find('.fax').text($('.fax-f').val());
        sel.find('.email').text($('.email-f').val());

    }

    table.on('click', '.form-sub', function () {

        submitVal('.selected');
        $('.add-btn').show();
        $('.selected').removeClass('selected');
        $(this).closest('tr').remove();
        $('.edit-btn').show();

    });

    var id = 1;
    $('.add-btn').on('click', function () {
        $('.delete-btn').addClass('disabled');
        $('.edit-btn').addClass('disabled');
        $(table).find('tr:eq(1)').before(`<tr class="new-form">
<form>
<td></td>
<td><input type="text" class="ragione-sociale-f" placeholder="Ragione Sociale"></td>
<td><input type="text" class="indirizzo-f" placeholder="Indirizzo"</td>
<td><input type="text" class="citta-f" placeholder="Città"></td>
<td><input type="text" class="provincia-f" placeholder="Provincia"></td>
<td><input type="text" class="cap-f" placeholder="CAP"></td>
<td><input type="text" class="piva-f" placeholder="PIVA"></td>
<td><input type="text" class="cf-f" placeholder="CF"></td>
<td><input type="text" class="telefono-f" placeholder="Telefono"></td>
<td><input type="text" class="fax-f" placeholder="Fax"></td>
<td><input type="text" class="email-f" placeholder="E-mail"></td>
<td><input class="new-row btn btn-outline-dark" type="submit" value="Save"></td>
<td><input class="cancel-btn btn btn-outline-dark" type="submit" value="Canc"></td>
<td></td>
</form>
</tr>`);

        $(this).hide();
    })

    table.on('click', '.new-row', function () {
        $('.edit-btn').removeClass('disabled');
        $('.delete-btn').removeClass('disabled');

        $('.add-btn').show();

        $(table).find('tr:eq(1)').before(`<tr class="new-tr">
    <td>${id}</td>
    <td class="ragione-sociale"></td>
    <td class="indirizzo"></td>
    <td class="citta"></td>
    <td class="provincia"></td>
    <td class="cap"></td>
    <td class="piva"></td>
    <td class="cf"></td>
    <td class="telefono"></td>
    <td class="fax"></td>
    <td class="email"></td>
    <td><button class="delete-btn btn btn-outline-dark">Delete</button></td>
    <td><button class="edit-btn btn btn-outline-dark">Edit</button></td>
    </tr>`).addClass('selected');

        id++;
        $('.new-tr').hide();
        submitVal('.new-tr');

        $(this).closest('tr').remove();
        $('.new-tr').show();
        $('.new-tr').removeClass('new-tr');

    })
});
