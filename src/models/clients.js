export class Clients {
    constructor(
        id,
        RagioneSociale,
        Indirizzo,
        Citta,
        Prov,
        CAP,
        PIVA,
        CF,
        Telefono,
        Fax,
        Email
    )
    {
      this.id = id;
      this.RagioneSociale = RagioneSociale;
      this.Indirizzo = Indirizzo;
      this.Citta = Citta;
      this.Prov = Prov;
      this.CAP = CAP;
      this.PIVA = PIVA;
      this.CF = CF;
      this.Telefono = Telefono;
      this.Fax = Fax;
      this.Email = Email    
    }
  }
  